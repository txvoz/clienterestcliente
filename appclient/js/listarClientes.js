function detalle(target){
    var id = $(target).data("id");
    window.location.replace("?p=detalleCliente&id="+id);
}

function eliminar(target) {
    var id = $(target).data("id");
    httpConnect(urlEndPoint +"/"+ id, null, "DELETE",
            function (r) {
                alert(r.message);
                cargarDatos();
            });
}

function cargarDatos() {
    var params = "?";
    if($("#identificacion").val()!==""){
        params += "identificacion="+$("#identificacion").val()+"&";
    }
    if($("#nombres").val()!==""){
        params += "nombres="+$("#nombres").val()+"&";
    }
    if($("#apellidos").val()!==""){
        params += "apellidos="+$("#apellidos").val()+"&";
    }
    if($("#edad").val()!==""){
        params += "filtroEdad="+$("#operador").val()+"_"+$("#edad").val()+"&";
    }
    httpConnect(urlEndPoint+params, null, "GET", function (r) {
        var html = "";
        $("tbody").html("");
        for (var i = 0; i < r.data.length; i++) {
            var e = r.data[i];
            var fechaNacimiento = new Date(e.fechaNacimiento);
            var now = new Date();
            var edad = (now - fechaNacimiento) / 31557600000;
            edad = Math.floor(edad);
            html += "<tr>";
            html += "<td>" + e.id + "</td>";
            html += "<td>" + e.tipoIdentificacion + "</td>";
            html += "<td>" + e.identificacion + "</td>";
            html += "<td>" + e.nombres + "</td>";
            html += "<td>" + e.apellidos + "</td>";
            html += "<td>" +fechaNacimiento.toLocaleDateString()+" / edad: "+ edad + "</td>";
            html += "<td>" + e.ciudadNacimiento + "</td>";
            html += "<td>";
            html += "<div data-id='" + e.id + "' class='material-icons delete' style='color:red'>delete</div>";
            html += "<div data-id='" + e.id + "' class='material-icons edit' style='color:green'>edit</div>";
            html += "</td>";
            html += "</tr>";
        }
        $("tbody").html(html);

        $(".delete").click(function () {
            if (confirm("Desea eliminar el recurso?")) {
                eliminar(this);
            }
        });
        $(".edit").click(function () {
            detalle(this);
        });

    });
}

$(function () {
    cargarDatos();
    $(".filtro").change(function(){
        cargarDatos();
    });
});