$(function(){
    $("#frmCrear").submit(function(){
        var cliente = new Object();

        cliente.tipoIdentificacion = $("#tipoIdentificacion").val();
        cliente.identificacion = $("#identificacion").val();
        cliente.nombres = $("#nombres").val();
        cliente.apellidos = $("#apellidos").val();
        cliente.fechaNacimiento = $("#fechaNacimiento").val();
        cliente.ciudadNacimiento = $("#ciudadNacimiento").val();
        
        var jCliente = JSON.stringify(cliente);
        
        httpConnect(urlEndPoint,jCliente,"POST",function(r){
            alert(r.message);
            $("button[type=reset]").click();
        });
        
        return false;
    });
});