var urlCognito = "https://pragma-app.auth.us-east-1.amazoncognito.com";
var client_id = "19vvk5bp0umjcofe029asq48ju";
var scope = "aws.cognito.signin.user.admin+email+openid+phone+profile";
var response_type = "token";
var redirect_uri = "http://localhost/app";

function getLoginURL(){
    var url = urlCognito+"/login";
    url += "?client_id="+client_id+"&";
    url += "response_type="+response_type+"&";
    url += "scope="+scope+"&";
    url += "redirect_uri="+redirect_uri+"/?p=login";
    return url;
}

function getLogoutURL(){
    var url = urlCognito+"/logout";
    url += "?client_id="+client_id+"&";
    url += "logout_uri="+redirect_uri+"/?p=logout";
    return url;
}

var urlEndPoint = "https://miaofxiuje.execute-api.sa-east-1.amazonaws.com/test/cliente";

$(function(){
    renderView();
});