function loadHTML(zona, obj, callback) {
    $.ajax({
        url: zona,
        success: function (html) {
            $(obj).html(html);
            var contentHtml = $(html);
            try {
                callback(contentHtml);
            } catch (e) {
                console.log(e);
            }
        },
        error: function (e, err) {
            alert("Error al cargar el html "+zona);
        }
    });
}

function getParameterByName(param) {
    //var that_url = location.search;
    var that_url = location.href.replace("#","&");
    param = param.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + param + "=([^&#]*)"),
            results = regex.exec(that_url);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function renderView() {
    loadHTML("template/master.html", $("body"), function (data) {
        //*******
        loadHTML("template/menu.html", $("#menu"), null);
        loadHTML("template/header.html", $("#cabecera"), null);
        loadHTML("template/footer.html", $("#pie_pagina"), null);

        var subpagina = "home.html";
        if (getParameterByName("p") !== "") {
            subpagina = getParameterByName("p") + ".html";
        }
        loadHTML("views/" + subpagina, $("#contenido_principal"), function(d){
            var token = localStorage.getItem("token");
            if(token){
                $(".activeLogin").show();
                $(".inactiveLogin").hide();
                $(".btnLogout").click(function(){
                    window.location.replace(getLogoutURL());
                });
            }else{
                $(".activeLogin").hide();
                $(".inactiveLogin").show();
                $(".btnLogin").click(function(){
                    window.location.replace(getLoginURL());
                });
            }
        });
        //*******
    });
}

function httpConnect(route, data, method, success = null, error = null, beforeSend = null, complete = null, require_auth = true) {

    var token = localStorage.getItem("token");
    
    var headers = {
        "X-COG-ID":token
    };
    $.ajax({
        type: method,
        url: route,
        contentType:"application/json",
        //context: data,
        data: data,
        //headers:headers,
        dataType: "json",
        beforeSend: function (request) {
            if(token){
                request.setRequestHeader("X-COG-ID", token);
            }
            $("#mask").addClass("show");
            console.log("Metodo:" + method);
            console.log(route, data, headers);
            if (beforeSend !== null) {
                beforeSend();
            }
        },
        complete: function () {
            $("#mask").removeClass("show");
            if (complete !== null) {
                complete();
            }
        },
        success: function (retorno) {
            $("#mask").removeClass("show");
            console.log(retorno);
            if (success !== null) {
                success(retorno);
            }
        },
        error: function (e, err) {
            $("#mask").removeClass("show");
            var status = e.status;
            var title = e.statusText;
            var body = "<br>" + route + "<br>" + e.responseText;
            console.error("httpError:c1:", e);
            console.error("httpError:c2:", err);
            var r = {
                "status": status,
                "title": title,
                "message": body
            };
            console.log(r);
            alert("Usted no esta autorizado para realizar esta accion, o se presento un error en la comunicacion. Gracias!");
            var token = localStorage.getItem("token");
            if(!token){
                window.location.replace("?");
            }
            //createMessage(r);
            if (error !== null) {
                error(e.responseText);
            }
        }
    });
}
