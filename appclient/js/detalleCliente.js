function cargarDetalle() {
    var id = getParameterByName("id");
    httpConnect(urlEndPoint+"/" + id, null, "GET",function(r){
        if(r.status!==200){
            alert(r.message);
            window.location.replace("?p=listarCategoria");
        }
        $("#tipoIdentificacion").val(r.data.tipoIdentificacion);
        $("#identificacion").val(r.data.identificacion);
        $("#nombres").val(r.data.nombres);
        $("#apellidos").val(r.data.apellidos);
        $("#fechaNacimiento").val(r.data.fechaNacimiento);
        $("#ciudadNacimiento").val(r.data.ciudadNacimiento);
        $("#id").val(id);
    },function(e){
        alert(e);
        window.location.replace("?p=listarClientes");
    });
}

$(function () {
    cargarDetalle();
    $("#frmUpdate").submit(function(){
        var entidad = new Object();
        
        entidad.tipoIdentificacion = $("#tipoIdentificacion").val();
        entidad.identificacion = $("#identificacion").val();
        entidad.nombres = $("#nombres").val();
        entidad.apellidos = $("#apellidos").val();
        entidad.fechaNacimiento = new Date($("#fechaNacimiento").val());
        entidad.ciudadNacimiento = $("#ciudadNacimiento").val();

        var jentidad = JSON.stringify(entidad);
        
        var id=$("#id").val();
        httpConnect(urlEndPoint+"/" + id,jentidad,"PUT",function(r){
            alert(r.message);
            window.location.replace("?p=listarClientes");
        });
        return false;
    });
});